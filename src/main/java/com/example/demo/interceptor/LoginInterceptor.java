package com.example.demo.interceptor;

import com.example.demo.pojo.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Mengxue
 * @date 2020/9/15 15:08
 * @version:
 * @Description: 拦截器。判断 session 中是否存在 user 属性，如果存在就放行，如果不存在就跳转到 login 页面
 * 在 Springboot 可以直接继承拦截器的接口，然后实现 preHandle 方法
 * 这种拦截器只有在将前后端项目整合在一起时才能生效
 */
public class LoginInterceptor implements HandlerInterceptor {

//    @Override
//    // preHandle 方法里的代码会在访问需要拦截的页面时执行
//    public boolean preHandle (HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
//        HttpSession session = httpServletRequest.getSession();
//        String contextPath = session.getServletContext().getContextPath();
//        String[] requireAuthPages = new String[]{
//                "index",
//        };
//
//        String uri = httpServletRequest.getRequestURI();
//        // 从 uri 中移除工程名 contextPath+"/"
//        uri = StringUtils.remove(uri, contextPath+"/");
//        String page = uri;
//
//        if (beginWith(page, requireAuthPages)) {
//            User user = (User) session.getAttribute("user");
//            if (user == null) {
//                httpServletResponse.sendRedirect("login");
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private boolean beginWith(String page, String[] requireAuthPages) {
//        boolean result = false;
//        for(String requireAuthPage : requireAuthPages) {
//            // 判断 page 是不是以 requireAuthPage 开头
//            if(StringUtils.startsWith(page, requireAuthPage)){
//                result = true;
//                break;
//            }
//        }
//        return result;
//    }
}
