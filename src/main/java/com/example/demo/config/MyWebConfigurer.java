package com.example.demo.config;

import com.example.demo.interceptor.LoginInterceptor;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author Mengxue
 * @date 2020/9/15 16:16
 * @version:
 * @Description: 拦截器配置类。将拦截器LoginInterceptor配置到项目中
 */
@SpringBootConfiguration
public class MyWebConfigurer implements WebMvcConfigurer {

//    @Bean
//    public LoginInterceptor getLoginInterceptor() {
//        return new LoginInterceptor();
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 对所有路径应用拦截器，除了 /index.html
//        registry.addInterceptor(getLoginInterceptor()).addPathPatterns("/**").excludePathPatterns("/index.html");
//    }

    // 所有请求都允许跨域
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        //所有请求都允许跨域
//        registry.addMapping("/**")
//                .allowedOrigins("*")
//                .allowedMethods("*")
//                .allowedHeaders("*");
//    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/api/file/**").addResourceLocations("file:" + "e:/Projects/vue-springboot-front_backend/img/");
    }
}
