package com.example.demo.Mapper;

import com.example.demo.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mengxue
 * @date 2020/9/15 13:32
 * @version:
 * @Description:
 */
public interface UserMapper extends JpaRepository<User,Integer> {
//  由于使用了 JPA，无需手动构建 SQL 语句，而只需要按照规范提供方法的名字即可实现对数据库的增删改查
    User findByUsername(String username);

    User getByUsernameAndPassword(String username, String password);
}
