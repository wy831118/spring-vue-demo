package com.example.demo.service;

import com.example.demo.Mapper.PositionMapper;
import com.example.demo.pojo.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/20 10:37
 * @version:
 * @Description:
 */
@Service
public class PositionService {
    @Autowired
    PositionMapper positionMapper;

    public List<Position> list() {
        return positionMapper.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public Position get(int id) {
        Position p = positionMapper.findById(id).orElse(null);
        return p;
    }
}
