package com.example.demo.service;

import com.example.demo.Mapper.DepartmentMapper;
import com.example.demo.pojo.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/20 10:26
 * @version:
 * @Description:
 */
@Service
public class DepartmentService {

    @Autowired
    DepartmentMapper departmentMapper;

    public List<Department> list() {
        return departmentMapper.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public Department get(int id) {
        Department d = departmentMapper.findById(id).orElse(null);
        return d;
    }
}
