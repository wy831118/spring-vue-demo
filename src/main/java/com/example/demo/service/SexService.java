package com.example.demo.service;

import com.example.demo.Mapper.SexMapper;
import com.example.demo.pojo.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/20 10:34
 * @version:
 * @Description:
 */
@Service
public class SexService {
    @Autowired
    SexMapper sexMapper;

    public List<Sex> list() {
        return sexMapper.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public Sex get(int id) {
        Sex s = sexMapper.findById(id).orElse(null);
        return s;
    }
}
