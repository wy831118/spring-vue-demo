# Springboot 

> Author: Mengxue

> Description: Backend of a [vue-demo project](https://gitlab.com/GuoAdeline/vue_demo).

## 访问前端页面
同时运行[vue-demo project](https://gitlab.com/GuoAdeline/vue_demo)和本项目，访问 http://localhost:8080/login 即可访问前端页面
## 整合前端
开发过程为前后端分离，但是为了实现后端登录拦截器，这里先将前后端整合，把前端打包后部署在后端。**这不是前后端分离项目推荐的做法**


首先在[vue-demo project](https://gitlab.com/GuoAdeline/vue_demo)项目目录执行
```
npm run build
```
这时在vue-demo项目的 `dist` 文件夹下生成了 `static` 文件夹和 `index.html` 文件，把这两个文件，拷贝到后端项目的 `spring-vue-demo\src\main\resources\static` 文件夹下

访问 http://localhost:8443/login ，成功进入登录页面
